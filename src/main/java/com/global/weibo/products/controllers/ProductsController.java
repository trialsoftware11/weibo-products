package com.global.weibo.products.controllers;

import com.global.weibo.products.model.Model;
import com.global.weibo.products.model.Product;
import com.global.weibo.products.services.ProductService;
import com.global.weibo.products.utils.InputParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/v1")
public class ProductsController {
    @Autowired
    InputParser inputParser;

    @Autowired
    ProductService productService;

    @PostMapping(value = "/searchProducts", produces = "application/json")
    public ResponseEntity<List<Model>> searchProducts(@RequestBody String queryString){
        List<Model> searchCriteria = inputParser.parseQueryString(queryString);
        List<Model> result = productService.searchProducts(searchCriteria);

        //If Found
        if(result != null && result.size()>0) {
            return ResponseEntity.ok(result);
        }else{ //If not found
            return ResponseEntity.ok(productService.saveProducts(searchCriteria));
        }

    }
}