package com.global.weibo.products.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.global.weibo.products.model.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class InputParser {

    Logger LOGGER = LoggerFactory.getLogger(InputParser.class);

    public List<Model> parseQueryString(String input) {
        List<Model> searchCriteria = new ArrayList<>();

        if(InputCheckerUtil.isCommaDelimitedString(input)){
            LOGGER.info("Comma Delimiter");
            searchCriteria.add(splitInstance(input));
        }
        else if(InputCheckerUtil.isMultipleInstances(input)){
            LOGGER.info("Multiple Instances "+input);
            String[] instances = input.split(Pattern.quote(InputCheckerUtil.INSTANCE_DELIMITER));
            for(String instance: instances){
                LOGGER.info("INstance "+instance);
                if(!instance.trim().isEmpty() && !instance.trim().equalsIgnoreCase(InputCheckerUtil.INSTANCE_DELIMITER)){

                    searchCriteria.add(splitInstance(instance.trim()));
                }
            }
        }
        else if(InputCheckerUtil.isJSON(input)){
            LOGGER.info("Is JSON");
            try {
                searchCriteria.add(new ObjectMapper().readValue(input, Model.class));
            }catch(Exception ex){
                ex.printStackTrace();
            }
        }
        else if(InputCheckerUtil.isXML(input)){
            LOGGER.info("Is XML");
            searchCriteria.add(getProductFromXml(input));
        }

        return searchCriteria;
    }

    private Model splitInstance(String input){
        LOGGER.info(input);
        String[] fields = input.split(",");
        return new Model(fields[0],fields[1],fields[2]);
    }

    private Model getProductFromXml(String input){
        try{
            JAXBContext jaxbContext = JAXBContext.newInstance(Model.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            return (Model) unmarshaller.unmarshal(new StringReader(input));
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return null;
    }
}