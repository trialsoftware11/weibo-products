package com.global.weibo.products.utils;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.global.weibo.products.model.Model;
import com.global.weibo.products.model.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;
import java.util.regex.Pattern;

public class InputCheckerUtil {

    public static final String INSTANCE_DELIMITER = "|";
    public static final Logger LOGGER = LoggerFactory.getLogger(InputCheckerUtil.class);

    public static boolean isXML(String input) {
        try {
            DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new StringReader(input)));
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean isJSON(String input) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            Model obj = mapper.readValue(input, Model.class);
            return true;
        }catch(Exception ex){
            return false;
        }
    }

    public static boolean isCommaDelimitedString(String input) {
        return !isJSON(input) && !isXML(input) & !isMultipleInstances(input) && input.contains(",");
    }

    public static boolean isMultipleInstances(String input) {
        return input.contains(INSTANCE_DELIMITER);
    }

}