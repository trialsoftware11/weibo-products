package com.global.weibo.products.repositories;

import com.global.weibo.products.model.Product;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface ProductRepository extends MongoRepository<Product, String> {
    @Query("{_id: '?0', name: '?1', categoryId: '?2'}")
    Product findByIdNameAndCategory(String _id, String name, String category);
}