package com.global.weibo.products.services;

import com.global.weibo.products.model.Category;
import com.global.weibo.products.model.Model;
import com.global.weibo.products.model.Product;
import com.global.weibo.products.repositories.CategoryRepository;
import com.global.weibo.products.repositories.ProductRepository;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductService {

    Logger LOGGER = LoggerFactory.getLogger(ProductService.class);

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    ProductRepository productRepository;

    public List<Model> searchProducts(List<Model> searchCriteria){
        List<Model> results = new ArrayList<>();
        try {
            for (Model criteria : searchCriteria) {
                //If the Product is blank in the input = do not save/output = no action
                if(criteria.getName().isEmpty()){
                    continue;
                }
                Category category = findCategory(criteria.getCategory());

                Product product = productRepository.findByIdNameAndCategory(criteria.getId(), criteria.getName(), category.get_id());
                if(product != null){
                    results.add(new Model(product.get_id(), product.getName(),category.getName()));
                }
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return results;
    }

    private Category findCategory(String name){
        Category category = categoryRepository.findByName(name);

        if(category==null){
            category = saveCategory(new Category(name));
        }
        return category;
    }

    public List<Model> saveProducts(List<Model> models){
        LOGGER.info("Inserting new product records");
        List<Model> savedItems = new ArrayList<>();
        for(Model model: models){
            Category category = findCategory(model.getCategory());
            Product product = new Product(model.getId(),model.getName(),category.get_id());
            product = productRepository.save(product);
            savedItems.add(new Model(product.get_id(),product.getName(), category.getName()));
        }
        return savedItems;
    }

    public Category saveCategory(Category category){
        LOGGER.info("Inserting new category records");
        return categoryRepository.save(category);
    }
}