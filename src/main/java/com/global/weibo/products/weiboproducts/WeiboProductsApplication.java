package com.global.weibo.products.weiboproducts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WeiboProductsApplication {

	public static void main(String[] args) {
		SpringApplication.run(WeiboProductsApplication.class, args);
	}

}
