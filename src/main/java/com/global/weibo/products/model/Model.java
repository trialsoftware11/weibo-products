package com.global.weibo.products.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "product")
public class Model {
    private String id;
    //The product name
    private String name;
    private String category;

    public Model() {

    }

    public Model(String id, String name, String category) {
        this.id = id;
        this.name = name;
        this.category = category;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}